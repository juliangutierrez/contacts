class Contact < ActiveRecord::Base
	belongs_to :state
	belongs_to :city

	def complete_name
		first_name + ' ' + last_name
	end
end
