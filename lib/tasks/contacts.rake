require 'csv'
namespace :contacts do
  desc "Create contacts"
  task :create => :environment do
  	file = File.open(Rails.root.to_s + '/lib/tasks/us-500.csv', "rb")
		data = file.read
  	csv = CSV.new(data, :headers => true, :header_converters => :symbol)
		csv.to_a.map {|row| row.to_hash }.each do |contact_hash|
			state_name = contact_hash[:state]
			state = State.find_or_create_by(name: state_name)
			contact_hash[:state] = state

			city_name = contact_hash[:city]
			city = City.find_or_create_by(name: city_name)
			contact_hash[:city] = city

			state.cities << city

			Contact.create contact_hash
		end
  end
end