class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
    	t.string :first_name  
    	t.string :last_name
    	t.string :company_name   
    	t.string :address   
    	t.integer :city_id  
    	t.string :county   
    	t.integer :state_id  
    	t.string :zip   
    	t.string :phone1   
    	t.string :phone2   
    	t.string :email   
    	t.string :web

      t.timestamps null: false
    end
  end
end
